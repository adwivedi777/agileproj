package  
{
        /**
         * ...
         * @author ashu
         */
        public class Player 
        {
                public var sign:String; //
                public var id:int;
                public function Player(id:int) 
                {
                        this.id = id;
                        trace("Player No : " + id + " created");
                }
                public function play():int                //it willl return the number of the dice face upwards
                {
                        var diceNum:int = 0;
                        trace("Player" + id +" turn");
                        trace("Roll the dice");
						var diceNum=rollDice();
                        return diceNum;
                }
        
		
				function rollDice()
				{
					var rand= Math.ceil(Math.random()*6);
					return rand;
				}
		}
}