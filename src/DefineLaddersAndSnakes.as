package  
{
        /**
         * ...
         * @author ashu
         */
        public class DefineLaddersAndSnake 
        {
                var titleCoordinates:Array = new Array();
				var tileSize:int=// size (length and height)of each tile in Px;
				var startTileNo1:int =0; //and so on
				var endTileNo1:int =0; //and so on

                public function DefineLaddersAndSnake (id:int)
                {
					//define where all the ladders are
					var ladders:Array = new Array();
					ladders[startTileNo1] = endTileNo1;	
					ladders[startTileNo2] = endTileNo2;	
					ladders[startTileNo3] = endTileNo3;
					//.....
					ladders[startTileNoN] = endTileNoN;

					//where startTileNo = tileNo of the start of the ladder, endTileNo is the end of
					//the ladder. So for example if your player goes from tile 5 to tile 30,
					//startTileNo = 5, endTileNo = 30;
					//Snakes are also 'ladders' since they move a  player from one tile to another
					//so for snakes, set the endTileNo  to be less than startTileNo.
					//create them using the same format
					//e.g ladders[25] = 15;
					
					
					//define where all the snakes are
					var snakes:Array = new Array();
					snakes[startTileNo1] = endTileNo1;	
					snakes[startTileNo2] = endTileNo2;	
					snakes[startTileNo3] = endTileNo3;
					//.....
					snakes[startTileNoN] = endTileNoN;
					
					
                              
                }
                
        }

}